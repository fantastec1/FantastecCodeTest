const { db } = require('./db');

const createUserFeatures = (email, location, feature) => {
    // if the enabledEmails matches the user email grant permission Or if the Ratio is 1

    if (feature.enabledEmails.includes(email)) {
        db.get('allocatedUserFeatures')
            .push({
                name: feature.name,
                email: email,
                location: location,
                allowed: true,
            })
            .write();

        return true;
    }

    // if the ratio is zero never allow
    if (!feature.ratio || feature.excludedCountries.includes(location)) {
        db.get('allocatedUserFeatures')
            .push({
                name: feature.name,
                email: email,
                location: location,
                allowed: false,
            })
            .write();

        return false;
    }
    // if the includeCoutries exists and doesnt include user country never allow
    if (
        feature.includedCountries.length > 0 &&
        !feature.includedCountries.includes(location)
    ) {
        db.get('allocatedUserFeatures')
            .push({
                name: feature.name,
                email: email,
                location: location,
                allowed: false,
            })
            .write();

        return false;
    }

    //if the ratio is one always allow

    if (feature.ratio === 1) {
        db.get('allocatedUserFeatures')
            .push({
                name: feature.name,
                email: email,
                location: location,
                allowed: true,
            })
            .write();

        return true;
    }

    //declare and create the ratio calc this is the occurance the feature is assigned
    var ratioCalc = 1 / feature.ratio;

    var featureHistory = db
        .get('allocatedUserFeatures')
        .filter({ name: feature.name })
        .value();

    // if (!featureHistory) featureHistory = [];

    var featureNotAssignCounter = 0;

    const latestfeatureAssignments = featureHistory.slice(-ratioCalc);
    //check how many times in the feature has been allocated to the latest ratio occurence of users

    for (var i = 0; i < latestfeatureAssignments.length; i++) {
        latestfeatureAssignments[i].allowed ? null : featureNotAssignCounter++;
    }

    if (featureNotAssignCounter === ratioCalc - 1) {
        db.get('allocatedUserFeatures')
            .push({
                name: feature.name,
                email: email,
                location: location,
                allowed: true,
            })
            .write();
        return true;
    } else {
        db.get('allocatedUserFeatures')
            .push({
                name: feature.name,
                email: email,
                location: location,
                allowed: false,
            })
            .write();
        return false;
    }
};

module.exports = { createUserFeatures };
