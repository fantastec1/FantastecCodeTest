const express = require('express');
const app = express();

const { db } = require('./db');

const { createUserFeatures } = require('./utils');

// parse requests of content-type - application/json
app.use(express.json({ limit: '50mb' }));

app.listen(3000, function () {
    console.log('Listening on port 3000');
});

app.post('/userFeatures', (req, res) => {
    const features = db.get('features').value();
    var currentUserFeatures = db
        .get('allocatedUserFeatures')
        .filter({ email: req.body.email, location: req.body.location })
        .value();

    const featuresAlreadyAsigned = currentUserFeatures.map(
        (userFeature) => userFeature.name
    );

    for (var i = 0; i < features.length; i++) {
        if (!featuresAlreadyAsigned.includes(features[i].name)) {
            createUserFeatures(req.body.email, req.body.location, features[i]);
        }
    }

    const newAllocatedUserFeatures = db
        .get('allocatedUserFeatures')
        .filter({ email: req.body.email, location: req.body.location })
        .value();

    res.send({ features: newAllocatedUserFeatures });
});
