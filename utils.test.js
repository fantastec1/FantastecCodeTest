const { createUserFeatures } = require('./utils');

const exampleFeatureWithEnabledEmail = {
    name: 'SuperCoolFeature',
    ratio: 0.5,
    enabledEmails: ['fred@example.com', 'mike@example.com'],
    includedCountries: ['GB'],
    excludedCountries: [],
};

const exampleFeatureWithExcludeCountry = {
    name: 'NewUserOnboardingJourney',
    ratio: 0.25,
    enabledEmails: ['tom@example.com'],
    includedCountries: [],
    excludedCountries: ['CA', 'GB'],
};

const exampleFeatureWithRatioOne = {
    name: 'MarketingBanner',
    ratio: 1,
    enabledEmails: [],
    includedCountries: ['US'],
    excludedCountries: [],
};

test('fred@example.com should have permission for SuperCoolFeature', () => {
    expect(
        createUserFeatures(
            'fred@example.com',
            'US',
            exampleFeatureWithEnabledEmail
        )
    ).toEqual(true);
});

test('With Location GB this should always return false', () => {
    expect(
        createUserFeatures(
            'fred@example.com',
            'GB',
            exampleFeatureWithExcludeCountry
        )
    ).toEqual(false);
});

test('With a ratio of 1 this should always return true', () => {
    expect(
        createUserFeatures(
            'SomeExample@example.com',
            'US',
            exampleFeatureWithRatioOne
        )
    ).toEqual(true);
});
